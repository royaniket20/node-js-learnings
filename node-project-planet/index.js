console.log(`************** PLANET PROJECT STARTUP *********************`);
const { parse } = require("csv-parse");
const { createReadStream } = require("fs");
//Stream Api will make you possible to stream data as it need
//Read a File to a readable stream  - Now you can create callbacks on fs events

function havitablePlanet(planet) {
  let decision =
    planet.koi_disposition === "CONFIRMED" &&
    planet.koi_insol > 0.36 &&
    planet.koi_insol < 1.11 &&
    planet.koi_prad < 1.6;
  return decision;
}
let havitablePlanetArr = [];
let readStream = createReadStream("./data/kepler_data.csv"); //This is the Byte Data we need to read in user redabale format
//Send the read stream toward o writable stream
readStream = readStream.pipe(
  parse({
    comment: "#",
    columns: true,
  })
);

readStream.on("data", (data) => {
  if (havitablePlanet(data)) {
    console.log(`Found a havitable planet - add to List - ${data.kepler_name}`);
    havitablePlanetArr.push(data);
  }
});

readStream.on("error", (error) => {
  console.log(`Error occured when reading File !`);
  console.log(error.message);
});

readStream.on("end", () => {
  console.log("Stream ended !");
  console.log(`Total Planets Found - ${havitablePlanetArr.length}`);
  //Map the Array result to more redable results 
  havitablePlanetArr.map(item => {
    return {id : item.kepid,
            formalName : item.kepoi_name,
            name : item.kepler_name}
  }).forEach(item=> {
    console.log(item);
  })
});
