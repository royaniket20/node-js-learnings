function decrypt(data) {
  return `Decrypted Data - ${data}`;
}

function read() {
  return decrypt("Data");
}

//module keyword can be used to export
export { read };
