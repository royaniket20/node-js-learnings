import { send, REQUEST_TIMEOUT } from "./request.mjs"; // Here only we are mentioning what we are using
import { read } from "./response.mjs"; //By Default it only look for js file
import { log } from "./singleElementExport.mjs"; //As this have Single Functuion exported we can direct;ly use it as you are returning Function
function myRequest(data, url) {
  send(url, data);
  return read();
}
log(`TimeOut - ${REQUEST_TIMEOUT}`);
log(myRequest("Url", "Some Data"));

//For ESM modules mention full file name with extension
