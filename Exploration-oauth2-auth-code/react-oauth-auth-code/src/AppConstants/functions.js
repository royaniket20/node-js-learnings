export function debounder(f) {
  let timeoutId;
  return function () {
    if (timeoutId) {
      clearTimeout(timeoutId);
    }
    timeoutId = setTimeout(() => {
      f();
    }, 500);
  };
}

export function doOAuthAuthCodeLogin(initUrl) {
  console.log(
    `Starting Oauth Login Process [Authorization Code Flow] ... with ${initUrl}`
  );
  console.log(`Lets try to do Simple Cros check with Health check Ping `);
  fetch("/api")
    .then((response) => response.json())
    .then((data) => console.log(data))
    .catch((error) => console.error(error))
    .finally(() => {
      console.log(`I am finally Done `);
      window.location.replace(initUrl);
    });
  console.log(`On Click Just Finished `);
}

export async function doOAuthAuthCodeLogout(logoutUrl) {
  console.log(
    `Starting Oauth lOGOUT  Process [Authorization Code Flow] ... with ${logoutUrl}`
  );
  localStorage.setItem("token", false);
  console.log(`On Click Just Finished  `);
  window.location.replace(logoutUrl);
}
