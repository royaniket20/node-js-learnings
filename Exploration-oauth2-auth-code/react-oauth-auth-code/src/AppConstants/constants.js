export const domain = "dev-hc8euv12o0fyh0r0.us.auth0.com";
export const audience = "http://www.challenges-api.com";
export const scope =
  "write:challenges read:challenges openid profile offline_access email";
export const clientId = "jM5XfnjqkQkiITXbOkduMqNq0ntMTtfW";
export const responseType = "code";
export const redirectUri = "http://localhost:5173/frontchannel";
export const returnTo = "http://localhost:5173/";
export const initUrl =
  `https://${domain}/authorize?` +
  `audience=${audience}&` +
  `scope=${scope}&` +
  `response_type=${responseType}&` +
  `client_id=${clientId}&` +
  `redirect_uri=${redirectUri}`;
  export const initLogOutUrl =
  `https://${domain}/logout?` +
  `client_id=${clientId}&` +
  `returnTo=${returnTo}`;
