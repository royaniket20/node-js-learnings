import {
  Navigate,
  Outlet,
  Route,
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
} from "react-router-dom";
import "./App.css";
import Home from "./components/Home";
import FrontChannel from "./components/FrontChannel";
import DashBoard from "./components/DashBoard";

function App() {
  const isExpired = () => {
    if (
      localStorage.getItem("token") &&
      localStorage.getItem("token") === "true"
    ) {
      return false;
    } else {
      return true;
    }
  };

  const AuthWrapper = () => {
    console.log("Checking is This page Visit allowed or Not ......");
    return isExpired() ? <Navigate to="/" replace /> : <Outlet />;
  };

  const RouteJsx = (
    <Route path="/">
      <Route path="" element={<Home />} />
      <Route path="home" element={<Home />} />
      <Route path="frontchannel" element={<FrontChannel />} />
      <Route element={<AuthWrapper />}>
        <Route path="dashboard" element={<DashBoard />} />
      </Route>
    </Route>
  );
  const routes = createRoutesFromElements(RouteJsx);
  const router = createBrowserRouter(routes);

  return <RouterProvider router={router} />;
}

export default App;
