/* eslint-disable react/prop-types */

export default function LogoutButton({onClick , label}) {
  return (
    <>    
<button onClick={onClick} className="rounded-xl bg-gradient-to-br from-[#FC00FF] via-[#504CF3] to-[#02FFD1] px-5 py-3 text-base font-medium text-white transition duration-200 hover:shadow-lg hover:shadow-[#504CF3]/50">
{label}
</button>
    </>
   
  )
}
