import { useEffect, useState } from "react";
import LogoutButton from "./LogoutButton";
import { debounder, doOAuthAuthCodeLogout } from "../AppConstants/functions";
import * as CONSTANT from "../AppConstants/constants.js";
export default function DashBoard() {
  const [challengeData, setChallengeData] = useState({
  });
  useEffect(() => {
    console.log(`Lets load the Data from Network .... `);
    fetch("/api")
      .then((response) => response.json())
      .then((data) => {
        setChallengeData((state) => {
          return { ...state, ...data };
        });
        console.log(data);
      })
      .catch((error) => console.error(error))
      .finally(() => {
        console.log(`I am finally Done calling health `);
      });

    fetch("/api/challenges")
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setChallengeData((state) => {
          return { ...state, ...data };
        });
      })
      .catch((error) => console.error(error))
      .finally(() => {
        console.log(`I am finally Done challege api call`);
      });
  }, []);
  return (
    <div className="flex flex-col h-dvh justify-evenly font-mono font-bold items-center text-5xl">
      <div>SOME CHALLENGE DATA - PROTECTED BY JWT </div>
      <div>{JSON.stringify(challengeData)}</div>

      <LogoutButton  onClick={debounder(() => doOAuthAuthCodeLogout(CONSTANT.initLogOutUrl))}
            label="Oath AuthrizationCode Flow LOGOUT"></LogoutButton>
    </div>
  );
}
