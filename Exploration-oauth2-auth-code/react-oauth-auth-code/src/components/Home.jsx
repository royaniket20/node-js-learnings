import LoginButton from "./LoginButton";
import * as CONSTANT from "../AppConstants/constants.js";
import { debounder, doOAuthAuthCodeLogin } from "../AppConstants/functions.js";

export default function Home() {
  

  return (
    <>
      <div className="h-dvh flex flex-col  justify-start items-center pt-3  bg-fuchsia-200">
        <h1 className="font-mono text-3xl">Hello Oauth Auth Code Flow</h1>
        <div className="m-4">
          <LoginButton
            onClick={debounder(() => doOAuthAuthCodeLogin(CONSTANT.initUrl))}
            label="Oath AuthrizationCode Flow LOGIN"
          ></LoginButton>
        </div>
      </div>
    </>
  );
}
