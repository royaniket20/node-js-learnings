import { useEffect, useState } from "react";
import queryString from "query-string";
import { useNavigate } from "react-router-dom";
import sleep from "sleep-promise";
import LoadingScreen from "./LoadingScreen";
export default function FrontChannel() {
  const navigate = useNavigate();
  const [isAccessTokenStoredInCookie, setIsAccessTokenStoredInCookie] =
    useState({ status: "running" });
  useEffect(() => {
    const { code } = queryString.parse(location.search);
    console.log(`Code captured from Url = ${code}`);
    if (code && isAccessTokenStoredInCookie.status === "running") {
      console.log(`Now we will get Access Token from Here .....`);
      //fetch(`http://localhost:3000/accessToken?code=${code}`)
      fetch(`/api/accessToken?code=${code}`)
        .then((response) =>
          response
            .json()
            .then((data) => ({ status: response.status, body: data }))
        )
        .then(sleep(2000))
        .then((data) => {
          console.log(`Data received ------ ${JSON.stringify(data)}`);
          if (data.status === 200)
            setIsAccessTokenStoredInCookie({ status: "success" });
          else throw new Error(JSON.stringify(data));
        })
        .catch((error) => {
          console.error(`Error happened ....... ${error.message}`);
          setIsAccessTokenStoredInCookie({ status: "error" });
        })
        .finally(() => {
          console.log(`I am finally Done `);
        });
    } else {
      setIsAccessTokenStoredInCookie({ status: "error" });
    }
  }, []);

  useEffect(() => {
    if (isAccessTokenStoredInCookie.status === "success") {
      localStorage.setItem("token", true);
      console.log(`Calling dahsboard .......`);
      navigate("/dashboard");
    } else if (isAccessTokenStoredInCookie.status === "error") {
      localStorage.setItem("token", false);
      navigate("/");
    } else {
      console.log(`Do Nothing as Nwk Call in progress ..... `);
    }
  }, [isAccessTokenStoredInCookie]);
  return <LoadingScreen></LoadingScreen>;
}
