const express = require("express");
const app = express();
const cors = require("cors");
const axios = require("axios");
const cookieParser = require("cookie-parser");
const { expressjwt: jwt } = require("express-jwt");
const jwks = require("jwks-rsa");
const guard = require("express-jwt-permissions")({
  requestProperty: "auth"
});
const port = 3000;

const corsOptions = {
  origin: "http://localhost:3000",
};

app.use(cors(corsOptions));
app.use(express.json());
app.get("/api/", (req, res) => {
  res.json({ health: "Up", date: new Date() });
});

const jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: "https://dev-hc8euv12o0fyh0r0.us.auth0.com/.well-known/jwks.json",
  }),
  getToken: (req) => {
    //console.log(`Accessing access_token value = ${req.cookies.access_token}`);
    return req.cookies.access_token;
  },
  audience: "http://www.challenges-api.com",
  issuerBaseURL: "https://dev-hc8euv12o0fyh0r0.us.auth0.com/",
  algorithms: ["RS256"],
});

// const errorHandler =   function (req, res, next) {
//   console.log(`Error handling middle ware ..... `);
//   // console.log(err);
//   // if(!err.code){
//     next();
//   // }else{

//   //   res.status(err.status).json({msg : err.code , date : new Date()});
//   // }
// };

app.use("/api/challenges", cookieParser());
//app.use("/api/challenges", errorHandler);
 app.use("/api/challenges", jwtCheck);


app.get("/api/challenges",  guard.check(["read:challenges"]),  (req, res) => {
 // console.log(req.auth);
  console.log(`Actual api is being called for result ------- `);
  res.json({
    challenge1: "This is the first challenge",
    challenge2: "This is another challenge",
  });
});

app.get("/api/accessToken", async (req, res) => {
  const auth_code = req.query.code;
  console.log(`Trying to fetch access Token with Auth Code  : ${auth_code}`);
  if (auth_code) {
    const tokenEndpoint =
      "https://dev-hc8euv12o0fyh0r0.us.auth0.com/oauth/token";
    const params = new URLSearchParams();
    params.append("grant_type", "authorization_code");
    params.append("client_id", "jM5XfnjqkQkiITXbOkduMqNq0ntMTtfW");
    params.append(
      "client_secret",
      "-YehsQJupJzIe-DhM72ZvIUVQi0VSdbYHFMr_fJrihs4Ry_cvQomcUoLMqwBi9l9"
    );
    params.append("code", auth_code);
    params.append("redirect_uri", "http://localhost:5173/frontchannel");
    let response = await axios
      .post(tokenEndpoint, params)
      .then((response) => {
        console.log(
          `Got Token from Auth Server  :  ${JSON.stringify(response.data)}`
        );
        return response;
      })
      .catch((error) => {
        console.log(
          "Error happend inAuth Server Token Generation " + error.message
        );
        console.log(error.response.data);
        console.log(error.response.status);
        return { data: error.message, status: error.response.status };
      });

    if (response.status === 200) {
      console.log(response.data);
      console.log(response.status);
      console.log(`Access Token = .${response.data.access_token}`);
      console.log(`Refresh Token  =  ${response.data.refresh_token}`);

      res
        .cookie("access_token", response.data.access_token, { httpOnly: true })
        .cookie("refresh_token", response.data.refresh_token, {
          httpOnly: true,
        })
        .json({
          msg: "Access Token and Refresh Token is Set in httpOnly Cookie",
          access_token: response.data.access_token,
          refresh_token: response.data.refresh_token,
          date: new Date(),
        });
    } else {
      //Error Scenario .....
      console.log(response.data);
      console.log(response.status);
      res
        .status(response.status)
        .json({ msg: response.data, date: new Date() });
    }
  } else {
    res
      .status(400)
      .json({ msg: "No Authrization Code is Sent ... ", date: new Date() });
  }
});

app.listen(port, () => {
  console.log(`Oauth Backend  listening on port ${port}`);
});
