const puppeteer = require('puppeteer');
async function run () {
  const url = "https://www.hdfcbank.com/";
  const browser = await puppeteer.launch({headless : "new"});
  const page = await browser.newPage();
  await page.goto(url);

  await page.screenshot({path: 'screenshot.png'});
  browser.close();
}
run();
let resuest = {
  body : {
    name : "aniket",
    age : 11 , 
    isMarried : true
  } , 
  param : {
    id : 100 , 
    status : "active"
  }
}

let  user = { 
  'name': 'Alex',
  'address': '15th Park Avenue',
  'age': 43,
  'department':{
      'name': 'Sales',
      'Shift': 'Morning',
      'address': {
          'city': 'Bangalore',
          'street': '7th Residency Rd',
          'zip': 560001
      }
  }
}

let {body : {age} , body : {id} } = resuest;
let {} = user;
console.log(age);
//https://www.freecodecamp.org/news/javascript-object-destructuring-spread-operator-rest-parameter/

 user = { 
  'name': 'Alex',
  'address': '15th Park Avenue',
  'age': 43
}

const clone = {...user} // Output, {name: "Alex", address: "15th Park Avenue", age: 43}

console.log(clone === user); // Output, false

// Add a new property salary
let updatedUser = {...user, salary:12345}; // {name: "Alex", address: "15th Park Avenue", age: 43, salary: 12345}

// Original object is unchanged
console.log(user , updatedUser); // {name: "Alex", address: "15th Park Avenue", age: 43}
 updatedUser = {...user, age:56}; // {name: "Alex", address: "15th Park Avenue", age: 56}

console.log(user , updatedUser); // {name: "Alex", address: "15th Park Avenue", age: 43}


user = { 
  'name': 'Alex',
  'address': '15th Park Avenue',
  'age': 43,
  'department':{
      'name': 'Sales',
      'Shift': 'Morning',
      'address': {
          'city': 'Bangalore',
          'street': '7th Residency Rd',
          'zip': 560001
      }
  }
}

let  updated = {
  ...user, 
  department: {'number': 7}
}

console.log(updated); //This is wrong 

updated = {
  ...user, 
  department: {
      ...user.department, 
      'number': 7,
      address : {
        ...user.department.address,
        'city': 'Kolkata',
        'village' : 'Bagnan'
        
      }
  }
};

console.log(updated);


 user = { 
  'name': 'Alex',
  'address': '15th Park Avenue',
  'age': 43
}

let  department = {
  'id': '001',
  'Shift': 'Morning'
}

const completeDetails = {...user, ...department};

console.log(completeDetails);
department = {
  'name': 'Sales', // This value will override user obj name 
  'Shift': 'Morning'
}
const overrideDetails = {...user, ...department};
console.log(overrideDetails);
user = { 
  'name': 'Alex',
  'address': '15th Park Avenue',
  'age': 43
}
const {age:ageData, ...rest} = user;
console.log(ageData, rest);




console.log(" This is a sample NodeJS Application");