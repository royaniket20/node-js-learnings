 let socket = io("https://lavender-airy-lupin.glitch.me");
//let socket = io("localhost:3000");
let overlay = document.getElementById("overlay");
let siteConn = document.getElementById("siteConn");
let connection = document.getElementById("connection");
let userName = document.getElementById("userName");
let userNameLabel = document.getElementById("userNameLabel");
let messagePad = document.getElementById("floatingTextareaForWriting");
let responsePad = document.getElementById("floatingTextareaForReading");
let userList = document.getElementById("privateUserName");
let currentUserName = "";
console.log(`Socket ready - ${socket}`);

connection.addEventListener("click", (event) => {
  console.log(`Clicked on connection -`);
  if (socket.connected) {
    currentUserName =
      userName.value.trim().length > 0 ? userName.value.trim() : "";
    if (currentUserName.length > 0) {
      socket.emit("sendName", { name: currentUserName, id: socket.id });
      showCustomToast(`Ready to start the communication`);
      setTimeout(() => {
        overlay.style.display = "none";
        hideNameTaker();
      }, 500);
    } else {
      showCustomToast(`😩 Enter your Name 😩`);
    }
  } else {
    showCustomToast(`Connection failed `);
  }
});

let messaging = document.getElementById("messaging");

messaging.addEventListener("click", (event) => {
  console.log(`Clicked on messaging - ${messagePad.value}`);
  socket.emit("sendMessage", { message: messagePad.value });
  messagePad.value = "";
});

console.log(`Event Listner added for connect `);
socket.on("connect", () => {
  showNameTaker();
  showCustomToast("Connected To the server");
    siteConn.innerHTML = "😎😍! Server is connecting ...  !😍😎";
    siteConn.innerHTML = "😎😍! Server Connected Back !😍😎";
});


let privatetalk = document.getElementById("privatetalk");

privatetalk.addEventListener("click", (event) => {
 
  let selectedUserId = userList.options[userList.selectedIndex].value;
  console.log(`Clicked on privatetalk - ${messagePad.value} | User - ${selectedUserId}`);
  if(selectedUserId.length>0){
    socket.emit("sendPrivatetalk", { message: messagePad.value , targetUserId : selectedUserId });
    messagePad.value = "";
  }else{
    showCustomToast("😡 Pick an User first 😡");
  }
  
});

socket.on("disconnect", () => {
  hideNameTaker();
  showCustomToast("Disconnected from server : reconnecting....");
  overlay.style.display = "flex";
  siteConn.innerHTML = "😖😖! Server Disconnected !😖😖";
});

socket.on('broadcastUsers' , (payload)=>{
  console.log(payload);
  console.log(`Updated Users - ${JSON.stringify(payload.messengers)}`);
  userList.innerHTML = '  <option value="">Pick a user </option>';
  payload.messengers.forEach(element => {
    let  opt = document.createElement("option");
    opt.value = element.id;
    opt.text = element.face+' '+element.name;
    if(socket.id !== element.id )
    {
      userList.add(opt); //Dont list yourself
    }
  });

  console.log(userList.options);
  
})

socket.on("broadcastedMessage", (payload) => {
  let audio = new Audio("ping-82822.mp3");
 
  console.log("Got the broadcast message....");
  console.log(payload);
  if (payload.msg.trim().length > 0) {
    playAudio(audio);
    responsePad.value =
      responsePad.value +
      "[Group : " +
      payload.userName +
      "] : " +
      payload.msg +
      "\n";
  }
  responsePad.scrollTop = responsePad.scrollHeight;
});

socket.on("receivePrivatetalk", (payload) => {
  let audio = new Audio("ping-82822.mp3");
  console.log("Got the private talk message....");
  console.log(payload);
  if (payload.content.trim().length > 0) {
    playAudio(audio);
    responsePad.value =
      responsePad.value +
      "[Private : " +
      payload.from +
      "] : " +
      payload.content +
      "\n";
  }
  responsePad.scrollTop = responsePad.scrollHeight;
});


function playAudio(audio) {
  audio.play();
}

function hideNameTaker() {
  userName.style.display = "none";
  connection.style.display = "none";
  userNameLabel.style.display = "none";
}
function  showNameTaker() {
  userName.style.display = "flex";
  connection.style.display = "flex";
  userNameLabel.style.display = "flex";
}

function showCustomToast(message) {
  Toastify({
    text: message,
    gravity: "top", // `top` or `bottom`
    position: "center", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(coral ,bisque )",
      color: "black",
      size: "30px",
      fontSize: "large",
      border: "thin solid black",
    },
    duration: 1500,
  }).showToast();
}
