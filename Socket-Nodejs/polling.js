/**
 const TIMER = 5000;
setInterval(() => {
  console.log(`Do the polling sync.....`);
  fetch("https://dummyjson.com/products/100")
    .then((res) => res.json())
    .then(console.log);
  console.log(`Setinterval finished sync`);
}, TIMER);

setInterval(async () => {
  console.log(`Do the polling async.....`);
  await fetch("https://dummyjson.com/products/100")
    .then((res) => res.json())
    .then(console.log);
  console.log(`Setinterval finished async`);
}, TIMER);

let caller = async function () {
  const completed = await new Promise((resolve, reject) => {
    const num = Math.random();
    if (num >= 0.5) {
      resolve("Promise is fulfilled!");
    } else {
      console.log("I am called ");
      reject("Promise failed!");
    }
  }).catch((err) => {
    console.log("Error happened "); //Or wrap promise in try catch
  });
  console.log(" Result - " + completed);
  return completed;
};

caller();
console.log(`I am already called`);
**/