Socket is used for real team bi directional communication 
Instead of Socket we can use some other alternatives 

Polling - making request to server repetadely.
But we do the polling blindly here Without Much knowledge when Data is available

But not good for real time application like Self driving car or Stock trading 

TCP Socket - Http Socket , WebSocket 
WebSocket - provide bidirection consistent connection 

Websocket is a protocol - many language have its own implementtion 

Nodejs - Ws Library implement it  for websocket server 
For websocket client  - Browser have native websocket Lib 

But there is a highlevel convient protocol which work om both frontend and backend 
---> Socket io Lib Implementation
Socket Io have consisten Api for both backend and frontend 

Socket Io on browser - will try to use Native Browser Websocket Lib but if rare case where 
Browser not supporting WebSocket native Obj - Socket io fall back to Polling using Http 

socket-io == for server 
socket-io-client = for client / Many language impls 

Socket io server and client have diff responsibility 

Client need to talk to only one server 
Server need to talk to many clients as needed

https://socket.io/docs/v4/emit-cheatsheet/

Installation node server for socket 

npm init -y  

We can use a standalone socket but better to use node http server so that we can use express later 

Namespace - A communication channel for split communication Like having many Groups where Client can connect for different taks 
Its upto develpers How to organize namespaces 

Namespace is nothing but a logical application communication channel over one physical Communication channel 

Namespace is Good - when you want to have multiple trypes of games hosted on same server - each seperated from one another 
If No Namespace is created  - default namespace is used 
Room is required for creating specific Game Lobby , Also it is good for Private chat also 

** Client can choose to send message to entire  current namespace and on server side we can catch the message and send to specific room 
Or we can  do like client use socket.join(room) / socket.leave(room) to Join the room and then send message to that Room directly 

