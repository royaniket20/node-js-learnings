const emoji = ['😀', '😁', '😂', '🤣', '😃', '😄', '😅', '😆', '😉', '😊', '😋', '😎', '😍', '😘', '😗', '😙', '😚', '☺', '🙂', '🤗', '🤔', '😐', '😑', '😶', '🙄', '😏', '😣', '😥', '😮', '🤐', '😯', '😪', '😫', '😴', '😌', '🤓', '😛', '😜', '😝', '🤤', '😒', '😓', '😔', '😕', '🙃', '🤑', '😲', '🙁', '😖', '😞', '😟', '😤', '😢', '😭', '😦', '😧', '😨', '😩', '😬', '😰', '😱', '😳', '😵', '😡', '😠', '😇', '🤠', '🤡', '🤥', '😷', '🤒', '🤕', '🤢', '🤧', '😈', '👿', '👹', '👺', '💀']
const express = require("express"); //this is a Function Import
const app = express(); //Calling the Function yo get the Event
app.get("/", (request, response) => {
  console.log(`Callig Health check Url  - ${request.url}`);
  response.send({ date: new Date().toUTCString(), name: "Node Js Socket IO Chat Server is Up" }); //Automatically add suitable  response content type
});

console.log(`Hello Pong Server ----`);

const server = require("http").createServer(app);
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
}); //attaching socket to our http server
const PORT = 3000;
//Data Store 
const messengers = new Map();
server.listen(PORT, () => {
  console.log(`Server started at Port - ${PORT}`);
});


io.on("connection", (socket) => {
  console.log(`Connected to the Client  -  ${socket.id}`);
  let userId = socket.id;
  messengers.set(userId,"");
  console.log(`Total Connected messeners - ${messengers.size}`);
  //SendName Event for registering to server
  socket.on("sendName", registerToServer); 
  //detedct client disconnect
  let fn = ()=> registerCleintDisconnect(userId);
  socket.on('disconnect',fn);
  let fn2 = (payload)=> broadcastToAll(userId,payload);
  //sendMessage Event for Sending all broadcast message
  socket.on("sendMessage", fn2); 
  let fn3 = (payload)=> sendPrivateChat(socket , userId,payload);
  socket.on("sendPrivatetalk" ,fn3 )
});




function registerCleintDisconnect(userId){
   console.log(`Client is disconnected from server ${messengers.get(userId)}`);
   messengers.delete(userId)
   console.log(`Total Connected messeners - ${messengers.size}`);
   emitUserListUpdate();
}
function registerToServer(payload)
{
    console.log(`Regestering player to the server`);
    console.log(payload);
    messengers.set(payload.id,payload.name);
    console.log(`Total Connected messeners - ${messengers.size}`);
    emitUserListUpdate();
  }

  function  sendPrivateChat(socket,userId , payload)
  {
      console.log(`Sending private chat to user .... `);
      console.log(payload);
      console.log(`Sending to User  - ${messengers.get(payload.targetUserId)}`);
    socket.to(payload.targetUserId).emit("receivePrivatetalk", {
    content : payload.message,
    from: messengers.get(userId),
  });
    }



function emitUserListUpdate() {
  let data = {
    messengers: []
  };
  let index = 0;
  messengers.forEach((v, k) => {
    if(v.length > 0){
      let user = { "id": k, "name": v  , "face" : emoji[index%emoji.length]};
      data.messengers.push(user);
      index++;
    }
  });
  console.log(`Sending data to all clients - `);
  console.log(data);
  io.emit('broadcastUsers', data);
}

  function broadcastToAll(userId,payload)
{
    console.log(`Capturing Message From Client - ${userId}`);
    let userName = messengers.get(userId) ?  messengers.get(userId) :  "Anonymous"
    let message = {
      userName : userName,
      msg : payload.message
    }
    console.log(message)
    console.log(`Sending bradcast message - ${message}`);
    io.emit('broadcastedMessage',message);
  }
  
 



