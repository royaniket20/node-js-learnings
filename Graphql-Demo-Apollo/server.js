const path = require("path");
const express = require("express");
const { ApolloServer } = require("apollo-server-express");
const { makeExecutableSchema } = require("@graphql-tools/schema");
const { loadFilesSync } = require("@graphql-tools/load-files");

async function startApolloServer() {
  //Fetched Schema Information from variouis module  and merge them
  const typeArr = loadFilesSync(path.join(__dirname, "**/*.graphql"));
  //Fetched Resolver Information from variouis module  and merge them
  const resolverArr = loadFilesSync(path.join(__dirname, "**/*.resolver.js"));
  //Create express app insrance
  const app = express();
  //Creating schema definatio Out of schemna + resolver
  const schemaDefination = makeExecutableSchema({
    typeDefs: typeArr,
    resolvers: resolverArr,
  });

  //create apollo server using schmea Def
  const server = new ApolloServer({
    schema: schemaDefination,
  });

  //Awating apollo server instance to start
  await server.start();
  //Apply Middleware to the server for a specific path
  server.applyMiddleware({
    app,
    path: "/graphql",
  });
  //Start listening to express server
  app.listen(3000, () => {
    console.log(`Graphql Node JS server is running on port : 3000`);
  });
}

startApolloServer();
