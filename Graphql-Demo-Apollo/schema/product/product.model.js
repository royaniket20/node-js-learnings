//Directly product : [] This Array is assigned to model export
const { v4: uuid } = require("uuid");
const staticProducts = [
  {
    id: "5af5f936-7cf8-4bcd-8455-46f8946b2c9e",
    description: "Kautzer, Padberg and Walter",
    price: 348.53,
    reviews: [
      { comment: "Wiegand, Schroeder and Yundt", rating: 3 },
      { comment: "Grant, Kassulke and Renner", rating: 5 },
      { comment: "Johns Inc", rating: 5 },
    ],
  },
  {
    id: "dc3690a3-196a-40cd-aee0-89e81c9ee717",
    description: "Kuhn Group",
    price: 389.94,
    reviews: [],
  },
  {
    id: "eea8b840-c1eb-4c44-abb9-68ba89a710c2",
    description: "Hand-King",
    price: 109.09,
    reviews: [{ comment: "Swaniawski-Hoeger", rating: 3 }],
  },
  {
    id: "14403ceb-8383-424d-9c9a-6164cd516b51",
    description: "Yost, Schimmel and Heidenreich",
    price: 330.03,
    reviews: [
      { comment: "Jast-Fritsch", rating: 4 },
      { comment: "Mante-Kessler", rating: 4 },
      { comment: "Feil-O'Hara", rating: 1 },
    ],
  },
  {
    id: "38c4d354-3d5c-494c-9789-6e8c1ead3f22",
    description: "Mueller Group",
    price: 386.07,
    reviews: [
      { comment: "Runolfsdottir-Cole", rating: 1 },
      { comment: "Dickens-Senger", rating: 4 },
      { comment: "Jaskolski-Collier", rating: 4 },
    ],
  },
  {
    id: "55cbee8d-766e-41ca-8d9d-ada3e09f9be8",
    description: "Gleichner, Welch and Stracke",
    price: 227.67,
    reviews: [
      { comment: "Becker, Bode and Streich", rating: 5 },
      { comment: "Parisian, Jerde and O'Connell", rating: 1 },
      { comment: "Wolf-Sauer", rating: 5 },
    ],
  },
  {
    id: "d933d16a-8f17-415b-99aa-e86c8e6410a5",
    description: "O'Keefe Group",
    price: 406.36,
    reviews: [
      { comment: "Nikolaus LLC", rating: 5 },
      { comment: "Grady, Feil and Ratke", rating: 4 },
      { comment: "Kertzmann, Langosh and Jacobson", rating: 3 },
    ],
  },
  {
    id: "a13b97d8-ac11-4dfb-b1c6-9cff2de187c2",
    description: "Wintheiser and Sons",
    price: 141.42,
    reviews: [],
  },
  {
    id: "7196ecc5-c046-4851-bc22-8cf4f0bb1ceb",
    description: "Kertzmann-Koepp",
    price: 405.61,
    reviews: [{ comment: "Toy-Farrell", rating: 5 }],
  },
  {
    id: "3ec86671-33ef-4e79-8e4e-c6ae7141f7ab",
    description: "Spencer, Stehr and Kris",
    price: 435.75,
    reviews: [{ comment: "Bergstrom LLC", rating: 3 }],
  },
];

function getAllProducts() {
  return staticProducts;
}

function productsByPriceRange(minPrice, maxPrice) {
  return staticProducts.filter(
    (item) => item.price >= minPrice && item.price <= maxPrice
  );
}

function productsById(id) {
  console.log(`Suppiled Id - ${id}`);
  let prodFiltered = staticProducts.find((item) => item.id == id);
  console.log(`Filtered product - ${JSON.stringify(prodFiltered)}`);
  return prodFiltered;
}

function addNewProduct(description, price) {
  const newProduct = {
    id: uuid(),
    description,
    price,
    reviews: [],
  };
  console.log(`Created rpoduct - ${newProduct}`);
  staticProducts.push(newProduct);
  return newProduct;
}

function addNewProductReview(id, rating, comment) {
  let prodFiltered = staticProducts.find((item) => item.id == id);
  if (prodFiltered) {
    review = {
      rating,
      comment,
    };
    prodFiltered.reviews.push(review);
  } else {
    throw Error("Product Not found with Id - " + id);
  }
  return review;
}
module.exports = {
  getAllProducts,
  staticProducts,
  productsByPriceRange,
  productsById,
  addNewProduct,
  addNewProductReview,
};
