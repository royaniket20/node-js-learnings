const { getAllProducts ,productsByPriceRange ,productsById ,addNewProduct , addNewProductReview } = require("./product.model");

module.exports = {
  Mutation : {
    addNewProduct : (_, args, context, info)=>{
       console.log(`Adding new Product`);
       return addNewProduct(args.description , args.price);
    },
    addNewProductReview : (_, args, context, info)=>{
        console.log(`Adding new Review to a Existing Product`);
        return addNewProductReview(args.id , args.rating,args.comment);
     }
  }  ,
  Query: {
    //async can be used or we can return promise
    products: (parent, args, context, info) => {
      console.log(`Calling products resolver `);
      return Promise.resolve(getAllProducts());
    },
    //Rule is in resolver if you are not using that argument - mark it using _
    productsByPriceRange : (_, args, __) =>{
        console.log(`Calling product filter by price range`);
        return Promise.resolve(productsByPriceRange(args.minPrice , args.maxPrice));
    },
    product(_,args,__){
        console.log(`Calling product filter by Id`);
        return  productsById(args.id);
    }
  },
};
