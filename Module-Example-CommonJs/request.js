const REQUEST_TIMEOUT = 500;
const READ_TIMEOUT = 500;
//This is private Function
function encrypt(data) {
  return `Ecrypted Data : ${data}`;
}

//This is public Method or exported method
function send(url, data) {
  let resp = encrypt(data);
  console.log(`Sending Encrypted Data to Url ${url} | Data - ${resp}`);
}

//module keyword can be used to export
module.exports = {
  send,
  REQUEST_TIMEOUT,
  READ_TIMEOUT,
};

console.log(`Initalizing the Request Module`);
