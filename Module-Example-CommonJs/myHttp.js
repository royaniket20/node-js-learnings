const { send, REQUEST_TIMEOUT } = require("./request.js"); // Here only we are mentioning what we are using
const resp = require("./response"); //By Default it only look for js file
const log = require("./singleElementExport.js"); //As this have Single Functuion exported we can direct;ly use it as you are returning Function
const { READ_TIMEOUT } = require("./request.js"); // Cache happening - So init code will not called again
function myRequest(data, url) {
  send(url, data);
  return resp.read();
}
log(`TimeOut - ${REQUEST_TIMEOUT}`);
log(myRequest("Url", "Some Data"));

//This have the cahe enetry of loaded Modules
console.log(require.cache);
