const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const { buildSchema } = require("graphql");
const allData = require('./MyData.json');
const {schemDef} = require('./schema.js');
const app = express();
const schemaDefination = buildSchema(schemDef);
const rootObj = {
  description: "This is a dummy product ",
  price: 99.569,
};
//This is a function Graphql Endpoint
app.use(
  "/graphql",
  graphqlHTTP({
    schema: schemaDefination,
    rootValue: allData,
    graphiql: true,
  })
);

app.listen(3000, () => {
  console.log(`Graphql Node JS server is running on port : 3000`);
});
