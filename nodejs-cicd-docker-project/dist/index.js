const express = require("express"); //this is a Function Import
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const app = express(); //Calling the Function yo get the Event
const swaggerJsdoc = require('swagger-jsdoc');

//TODO - For swagger Json Update 
//swagger-jsdoc -d swagger-def.js index.js


const PORT =  process.env.PORT ?? 8000;

app.listen(PORT, () => {
  console.log(`Server started on ${PORT}`);
});

/**
 * @swagger
 * /:
 *   get:
 *     summary: Retrive server root path response
 *     description: Retrive server root path response
 *     responses:
 *       200:
 *         description: Simple response .
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 */
app.get("/", (request, response) => {
  console.log(`Callig - ${request.url}`);
  response.json({
    message: "Hello from Express Server 👋🏻",
  })
});

/**
 * @swagger
 * /health:
 *   get:
 *     summary: Retrive server health path response
 *     description: Retrive server health path response
 *     responses:
 *       200:
 *         description: Simple health response .
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 */
app.get("/health", (request, response) => {
  console.log(`Callig - ${request.url}`);
  response.json({ message: "Everything is healthy 👀" })
});

const friends = [
  {
    id: 100,
    name: "Aniket Roy",
  },
  {
    id: 200,
    name: "Aniket Pal",
  },
  {
    id: 300,
    name: "Aniket Sen",
  },
  {
    id: 400,
    name: "Aniket Das",
  },
];

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

//RREMEMBER - ORDER OF MIDDLEWARE ALWAYS MATTERS 

//Write our Own Middle Ware 

app.use((req , resp , next) =>{
  console.log(`I am inside  middleware 1`);
  const start= Date.now();
  next(); //Here Now this will go to actual endpoint for processing
  const end= Date.now();
  console.log(`${req.url} --> ${req.method} | Time taken - ${end-start} ms`);
  console.log(`I am coming out of  middleware 1`);

});

app.use((req , resp , next) =>{
 console.log(`I am inside  middleware 2`);
  next(); //Here Now this will go to actual endpoint for processing
  console.log(`I am coming out of middleware 2`);
});

//Json parsing middleware - Built In 
app.use(express.json());
/**
 * @swagger
 * /friends:
 *   get:
 *     summary: Retrive server Friends path response
 *     description: Retrive server Friends path response
 *     responses:
 *       200:
 *         description: Simple Friends response .
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                type: object
 *                properties:
 *                  id:
 *                   type: integer
 *                  name:
 *                   type: string
 */
app.get("/friends", (request, response) => {
  console.log(`Callig - ${request.url}`);
  response.send(friends); //Automatically add suitable  response content type
});

/**
 * @swagger
 * /friends/{firendId}:
 *   get:
 *     summary: Retrive server Friends By Id  path response
 *     description: Retrive server Friends By Id path response
 *     parameters:
 *       - in: path
 *         name: firendId
 *         required: true
 *         description: Numeric ID of the friend to retrieve.
 *         schema:
 *           type: integer
 *     responses:
 *       200:
 *         description: Simple Friends Single response .
 *         content:
 *           application/json:
 *             schema:
 *                type: object
 *                properties:
 *                  id:
 *                   type: integer
 *                  name:
 *                   type: string
 */
app.get("/friends/:firendId", (request, response) => {
  console.log(`Callig - ${request.url}`);
  let firendId = Number(request.params.firendId);
  let friend = friends.find((item) => item.id === firendId);
  if (friend) {
    response.send(friend);
  } else {
    response
      .status(404)
      .json({ msg: "Friend Not Found with Id - " + request.params.firendId });
  }
  //Automatically add suitable  response content type
});


/**
 * @swagger
 * /friends:
 *   post:
 *     summary: Retrive server save friend path response
 *     description: Retrive server save friend path response
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: The user's name.
 *                 example: Leanne Graham
 *     responses:
 *       201:
 *         description: Simple Friends Save Single response .
 *         content:
 *           application/json:
 *             schema:
 *                type: object
 *                properties:
 *                  id:
 *                   type: integer
 *                  name:
 *                   type: string
 */
app.post("/friends", (request, response) => {
  console.log(`Callig - ${request.url}`);
  const newFriend = {
    id : friends.length,
    name : request.body.name
  } ;
  console.log(`New Friend ADDED  -  ${JSON.stringify(newFriend)}`);
   friends.push(newFriend);
  if (request.body.name) {
    response.status(201).send(newFriend);
  } else {
    response
      .status(400)
      .json({ msg: "Invalid Payload Sent- " +  JSON.stringify(request.body) });
  }
  //Automatically add suitable  response content type
});


