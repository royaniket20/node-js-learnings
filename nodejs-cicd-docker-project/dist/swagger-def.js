module.exports =   {
    openapi: '3.0.0',
    info: {
      title: 'Express API for Hello World NodeJS CI CD ',
      version: '1.0.0',
      description:
        'This is a REST API application made with Express.',
      license: {
        name: 'Licensed Under MIT',
        url: 'https://spdx.org/licenses/MIT.html',
      },
      contact: {
        name: 'Aniket Roy',
        url: 'https://www.linkedin.com/in/aniket-roy-a9529272/',
      },
    },
    servers: [
      {
        url: 'http://localhost:8000',
        description: 'Local server',
      },
      {
        url: 'http://localhost:8000',
        description: 'Cloud server',
      },
    ],
  };