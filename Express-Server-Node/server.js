const express = require("express"); //this is a Function Import

const app = express(); //Calling the Function yo get the Event

const PORT = 3000;

app.listen(PORT, () => {
  console.log(`Server started on ${PORT}`);
});

app.get("/", (request, response) => {
  console.log(`Callig - ${request.url}`);
  response.send("Hello !!!");
});

app.get("/json", (request, response) => {
  console.log(`Callig - ${request.url}`);
  response.send({ id: 100, name: "Aniket Roy" }); //Automatically add suitable  response content type
});

const friends = [
  {
    id: 100,
    name: "Aniket Roy",
  },
  {
    id: 200,
    name: "Aniket Pal",
  },
  {
    id: 300,
    name: "Aniket Sen",
  },
  {
    id: 400,
    name: "Aniket Das",
  },
];

//Write our Own Middle Ware 

app.use((req , resp , next) =>{
  console.log(`I am inside  middleware 1`);
  const start= Date.now();
  next(); //Here Now this will go to actual endpoint for processing
  const end= Date.now();
  console.log(`${req.url} --> ${req.method} | Time taken - ${end-start} ms`);
  console.log(`I am coming out of  middleware 1`);

});

app.use((req , resp , next) =>{
 console.log(`I am inside  middleware 2`);
  next(); //Here Now this will go to actual endpoint for processing
  console.log(`I am coming out of middleware 2`);
});

//Json parsing middleware - Built In 
app.use(express.json());

app.get("/friends", (request, response) => {
  console.log(`Callig - ${request.url}`);
  response.send(friends); //Automatically add suitable  response content type
});

app.get("/friends/:firendId", (request, response) => {
  console.log(`Callig - ${request.url}`);
  let firendId = Number(request.params.firendId);
  let friend = friends.find((item) => item.id === firendId);
  if (friend) {
    response.send(friend);
  } else {
    response
      .status(404)
      .json({ msg: "Friend Not Found with Id - " + request.params.firendId });
  }
  //Automatically add suitable  response content type
});



app.post("/friends", (request, response) => {
  console.log(`Callig - ${request.url}`);
  const newFriend = {
    id : friends.length,
    name : request.body.name
  } ;
  console.log(`New Friend ADDED  -  ${JSON.stringify(newFriend)}`);
   friends.push(newFriend);
  if (request.body.name) {
    response.send(newFriend);
  } else {
    response
      .status(400)
      .json({ msg: "Invalid Payload Sent- " +  JSON.stringify(request.body) });
  }
  //Automatically add suitable  response content type
});


