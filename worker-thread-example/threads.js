const {Worker , isMainThread, workerData} = require('worker_threads')

if(isMainThread){
  console.log(`I am now a Main thread - ${process.pid}`);
  new Worker(__filename , {
    workerData : [1,5,8,2]
  }); 
  new Worker(__filename ,
    {
      workerData : [4,8,0,1]
    });
  //use the Current File -- Will crash eventually if Not only called from Main thread 
}else{
  console.log(`I am now a worker thread - ${process.pid} with Worker Data - ${workerData}`);
}
//Worker thread will have same Pid as its belongs to same process 