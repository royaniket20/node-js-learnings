let options = {
  method: "POST",
  headers: {
    "Content-Type": "text/plain",
  },
  body: "Hello Aniket",
};
// Fake api for making post requests
let fetchRes = fetch("http://localhost:3002/", options);

fetchRes
.then(res =>
  res.text()) //Converting the Readable Stream to Text 
.then((data) => {
  console.log(`Received Data - ${data}`);
});
