console.log(`--- NODE SERVER ---------------`);

const http = require("http");
const PORT = 3000;
const PORT2 = 3001;
const PORT3 = 3002;
//These will always Server to Root Path /
//Server obj getting created
const server = http.createServer((request, response) => {
  response.writeHead(200, {
    "Content-Type": "text/plain",
  });
  response.end("Server is Up and Running !!");
});

server.listen(PORT, () => {
  console.log(`Started Server on Port ${PORT}`);
});

//Server obj getting created
const server2 = http.createServer((request, response) => {
  response.writeHead(200, {
    "Content-Type": "application/json",
  });
  response.end(JSON.stringify({ health: "Server is Up and Running !!" }));
});

server2.listen(PORT2, () => {
  console.log(`Started Server on Port ${PORT2}`);
});

//Example of Path based Routhing
const server3 = http.createServer((request, response) => {
  if (request.url === "/json") {
    response.writeHead(200, {
      "Content-Type": "application/json",
    });
    response.end(JSON.stringify({ health: "Server is Up and Running !!" }));
  } else if (request.url === "/text") {
    response.writeHead(200, {
      "Content-Type": "text/plain",
    });
    response.end("Server is Up and Running !!");
  } else if (request.url === "/html") {
    response.writeHead(200, {
      "Content-Type": "text/html",
    });
    response.write(
      "<html><body><h1>Server is Up and Running !!</h1></body></html>"
    );
    response.end();
  } else if (request.method === "POST") {
    //REQUEST IS A REDABLE STREAM
    //RESPONSE IS WRITABLE STREAM
    let dataCaptured = null;
    request.on("data", (data) => {
      console.log(`Request Recived - ${data.toString()}`);
      dataCaptured = data.toString();
    });

    //Instead of using Close event - we can use Pipe also
    // request.on("close", () => {
    //   console.log(`Request is completely Received !! Sending Response ${dataCaptured}`);
    //   response.writeHead(200, {
    //     "Content-Type": "text/plain",
    //   });
    //   response.end(dataCaptured);
    // });

    //Use Pipe  to Send data from requet to response
    //This will wait Until the Request is completed and Request Close event is emitted
    request.pipe(response);
  } else {
    response.statusCode = 404;
    response.setHeader("Content-Type", "application/json");
    response.end(JSON.stringify({ health: "No Valid Path Found" }));
  }
});

server3.listen(PORT3, () => {
  console.log(`Started Server on Port ${PORT3}`);
});
