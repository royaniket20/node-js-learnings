//Directly order : [] This Array is assigned to model export

const staticOrders = [
  {
    date: "2023-03-29T11:59:53Z",
    subtotal: 21.38,
    items: [
      {
        quantity: 2,
        product: {
          id: "38c4d354-3d5c-494c-9789-6e8c1ead3f22",
          description: "Mueller Group",
          price: 386.07,
        },
      },
      {
        quantity: 2,
        product: {
          id: "14403ceb-8383-424d-9c9a-6164cd516b51",
          description: "Yost, Schimmel and Heidenreich",
          price: 330.03,
        },
      },
      {
        quantity: 2,
        product: {
          id: "d933d16a-8f17-415b-99aa-e86c8e6410a5",
          description: "O'Keefe Group",
          price: 406.36,
        },
      },
      {
        quantity: 2,
        product: {
          id: "dc3690a3-196a-40cd-aee0-89e81c9ee717",
          description: "Kuhn Group",
          price: 389.94,
        },
      },
    ],
  },
  {
    date: "2023-07-22T14:05:27Z",
    subtotal: 78.44,
    items: [
      {
        quantity: 1,
        product: {
          id: "5af5f936-7cf8-4bcd-8455-46f8946b2c9e",
          description: "Kautzer, Padberg and Walter",
          price: 348.53,
        },
      },
      {
        quantity: 3,
        product: {
          id: "eea8b840-c1eb-4c44-abb9-68ba89a710c2",
          description: "Hand-King",
          price: 109.09,
        },
      },
      {
        quantity: 1,
        product: {
          id: "a13b97d8-ac11-4dfb-b1c6-9cff2de187c2",
          description: "Wintheiser and Sons",
          price: 141.42,
        },
      },
    ],
  },
  {
    date: "2023-11-01T07:50:04Z",
    subtotal: 53.46,
    items: [
      {
        quantity: 1,
        product: {
          id: "55cbee8d-766e-41ca-8d9d-ada3e09f9be8",
          description: "Gleichner, Welch and Stracke",
          price: 227.67,
        },
      },
      {
        quantity: 3,
        product: {
          id: "7196ecc5-c046-4851-bc22-8cf4f0bb1ceb",
          description: "Kertzmann-Koepp",
          price: 405.61,
        },
      },
      {
        quantity: 3,
        product: {
          id: "3ec86671-33ef-4e79-8e4e-c6ae7141f7ab",
          description: "Spencer, Stehr and Kris",
          price: 435.75,
        },
      },
    ],
  },
  {
    date: "2023-01-20T04:43:06Z",
    subtotal: 35.98,
    items: [
      {
        quantity: 3,
        product: {
          id: "14403ceb-8383-424d-9c9a-6164cd516b51",
          description: "Yost, Schimmel and Heidenreich",
          price: 330.03,
        },
      },
      {
        quantity: 2,
        product: {
          id: "5af5f936-7cf8-4bcd-8455-46f8946b2c9e",
          description: "Kautzer, Padberg and Walter",
          price: 348.53,
        },
      },
      {
        quantity: 2,
        product: {
          id: "d933d16a-8f17-415b-99aa-e86c8e6410a5",
          description: "O'Keefe Group",
          price: 406.36,
        },
      },
    ],
  },
  {
    date: "2023-07-30T16:53:53Z",
    subtotal: 93.86,
    items: [
      {
        quantity: 2,
        product: {
          id: "55cbee8d-766e-41ca-8d9d-ada3e09f9be8",
          description: "Gleichner, Welch and Stracke",
          price: 227.67,
        },
      },
    ],
  },
  {
    date: "2022-12-23T12:33:15Z",
    subtotal: 62.38,
    items: [
      {
        quantity: 2,
        product: {
          id: "38c4d354-3d5c-494c-9789-6e8c1ead3f22",
          description: "Mueller Group",
          price: 386.07,
        },
      },
      {
        quantity: 3,
        product: {
          id: "a13b97d8-ac11-4dfb-b1c6-9cff2de187c2",
          description: "Wintheiser and Sons",
          price: 141.42,
        },
      },
      {
        quantity: 3,
        product: {
          id: "eea8b840-c1eb-4c44-abb9-68ba89a710c2",
          description: "Hand-King",
          price: 109.09,
        },
      },
      {
        quantity: 3,
        product: {
          id: "dc3690a3-196a-40cd-aee0-89e81c9ee717",
          description: "Kuhn Group",
          price: 389.94,
        },
      },
      {
        quantity: 2,
        product: {
          id: "3ec86671-33ef-4e79-8e4e-c6ae7141f7ab",
          description: "Spencer, Stehr and Kris",
          price: 435.75,
        },
      },
    ],
  },
  {
    date: "2023-11-22T11:58:43Z",
    subtotal: 82.37,
    items: [
      {
        quantity: 2,
        product: {
          id: "7196ecc5-c046-4851-bc22-8cf4f0bb1ceb",
          description: "Kertzmann-Koepp",
          price: 405.61,
        },
      },
      {
        quantity: 3,
        product: {
          id: "eea8b840-c1eb-4c44-abb9-68ba89a710c2",
          description: "Hand-King",
          price: 109.09,
        },
      },
      {
        quantity: 2,
        product: {
          id: "5af5f936-7cf8-4bcd-8455-46f8946b2c9e",
          description: "Kautzer, Padberg and Walter",
          price: 348.53,
        },
      },
      {
        quantity: 3,
        product: {
          id: "a13b97d8-ac11-4dfb-b1c6-9cff2de187c2",
          description: "Wintheiser and Sons",
          price: 141.42,
        },
      },
    ],
  },
  {
    date: "2023-02-04T18:39:13Z",
    subtotal: 94.04,
    items: [
      {
        quantity: 3,
        product: {
          id: "dc3690a3-196a-40cd-aee0-89e81c9ee717",
          description: "Kuhn Group",
          price: 389.94,
        },
      },
      {
        quantity: 2,
        product: {
          id: "38c4d354-3d5c-494c-9789-6e8c1ead3f22",
          description: "Mueller Group",
          price: 386.07,
        },
      },
      {
        quantity: 1,
        product: {
          id: "55cbee8d-766e-41ca-8d9d-ada3e09f9be8",
          description: "Gleichner, Welch and Stracke",
          price: 227.67,
        },
      },
      {
        quantity: 3,
        product: {
          id: "7196ecc5-c046-4851-bc22-8cf4f0bb1ceb",
          description: "Kertzmann-Koepp",
          price: 405.61,
        },
      },
    ],
  },
  {
    date: "2023-08-10T20:40:06Z",
    subtotal: 97.47,
    items: [
      {
        quantity: 3,
        product: {
          id: "d933d16a-8f17-415b-99aa-e86c8e6410a5",
          description: "O'Keefe Group",
          price: 406.36,
        },
      },
    ],
  },
  {
    date: "2023-04-20T10:48:09Z",
    subtotal: 86.12,
    items: [
      {
        quantity: 2,
        product: {
          id: "14403ceb-8383-424d-9c9a-6164cd516b51",
          description: "Yost, Schimmel and Heidenreich",
          price: 330.03,
        },
      },
      {
        quantity: 3,
        product: {
          id: "3ec86671-33ef-4e79-8e4e-c6ae7141f7ab",
          description: "Spencer, Stehr and Kris",
          price: 435.75,
        },
      },
    ],
  },
];

function getAllOrders(){
  return staticOrders;
}

module.exports = {getAllOrders ,staticOrders}
