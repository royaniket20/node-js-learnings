const path = require("path");
const express = require("express");
const { graphqlHTTP } = require("express-graphql");
//const { buildSchema } = require("graphql");
// const allData = require("./MyData.json");
// const {schemDef} = require('./schema.js');
const { makeExecutableSchema } = require("@graphql-tools/schema");
const { loadFilesSync } = require("@graphql-tools/load-files");
const { arch } = require("os");
const app = express();

//Special Note - ** = Looks for all Folders and SubFolders from current Point
/**
 * parent = rootValue - which is set to express router
 * args = for filtering criteria
 * context = shared context of all resolvers  - authentication
 * info = Info about current state of the execution
 */
const typeArr = loadFilesSync(path.join(__dirname, "**/*.graphql"));
const resolverArr = loadFilesSync(path.join(__dirname, "**/*.resolver.js"));
const schemaDefination = makeExecutableSchema({
  typeDefs: typeArr,
  resolvers: resolverArr,
  // resolvers: {
  //   Query: {
  //     //async can be used or we can return promise
  //     products: (parent, args, context, info) => {
  //       console.log(`Calling products resolver `);
  //       return Promise.resolve(parent.products);
  //     },
  //     orders: async (parent, args, context, info) => {
  //       console.log(`Calling orders resolver `);
  //       return await Promise.resolve(parent.orders);
  //     },
  //   },
  // },
});
// const schemaDefination = buildSchema(schemDef);
const rootObj = {
  products: require("./schema/product/product.model").staticProducts,
  orders: require("./schema/order/order.model").staticOrders,
};
//This is a function Graphql Endpoint
app.use(
  "/graphql",
  graphqlHTTP({
    schema: schemaDefination,
    //Commented because resolver is Now handling the data fetch
    //rootValue: allData,
    // rootValue: rootObj,
    graphiql: true,
  })
);

app.listen(3000, () => {
  console.log(`Graphql Node JS server is running on port : 3000`);
});
