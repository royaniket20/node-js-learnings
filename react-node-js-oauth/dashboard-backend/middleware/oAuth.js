var axios = require("axios");

const tokenEndpoint = "https://dev-hc8euv12o0fyh0r0.us.auth0.com/oauth/token";

oAuth = async (req, res, next) => {
  let code = req.query.code;
  console.log(` Code captured --------------------` + code);
  if (!code) {
    res.status(401).send("Missing authorization code");
  }

  const params = new URLSearchParams();
  params.append("grant_type", "authorization_code");
  params.append("client_id", "jM5XfnjqkQkiITXbOkduMqNq0ntMTtfW");
  params.append(
    "client_secret",
    "-YehsQJupJzIe-DhM72ZvIUVQi0VSdbYHFMr_fJrihs4Ry_cvQomcUoLMqwBi9l9"
  );
  params.append("code", code);
  params.append("redirect_uri", "http://localhost:3000/challenges");

  let response = await axios
    .post(tokenEndpoint, params)
    .then((response) => {
      console.log(`get data in middleware ------ ${JSON.stringify(response.data)}`);
      return response.data;
    })
    .catch((err) => {
       console.log("Error happend in Middleware section ------------" + err.message);
      return {"Reason" : err.message};
    });
    req.oauth = response;
    console.log(`Calling the actual api ......` + response);
     next();
    console.log(`Returning from actual api call ......`);
};

module.exports = oAuth;
