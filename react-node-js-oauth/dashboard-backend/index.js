var express = require("express");
var axios = require("axios");
var port = process.env.PORT || 3001;
var oAuth = require("./middleware/oAuth");
var app = express();

const challengesAPIEndpoint = "http://localhost:8080/challenges";


app.use(oAuth);

app.get("/challenges", async (req, res) => {
  try {
    console.log(`Calling dashboard backend -------`);
    const { access_token } = req.oauth;
    console.log(`Retrived Access Token ====== ${access_token}`);
    const response = await axios({
      method: "get",
      url: challengesAPIEndpoint,
      headers: { Authorization: `Bearer ${access_token}` },
    });
    console.log(`Got the result -------- ${JSON.stringify(response.data)}`);
    res.status(200).end(JSON.stringify(response.data));
  } catch (error) {
    console.log(error);
    res.status(401).json({"status" : "Unauthorized to access data"});
    // if (error.response.status === 401) {
    //   res.status(401).json("Unauthorized to access data");
    // } else if (error.response.status === 403) {
    //   res.status(403).json("Permission denied");
    // } else {
    //   res.status(500).json("Whoops, something went wrong");
    // }
  }
});

app.listen(port, () => console.log("Started at Port = "+ port));
