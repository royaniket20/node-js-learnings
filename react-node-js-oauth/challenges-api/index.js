var express = require("express");
var app = express();
var jwt = require("express-jwt");
var jwks = require("jwks-rsa");
var guard = require("express-jwt-permissions")();

var port = process.env.PORT || 8080;

var jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: "https://dev-hc8euv12o0fyh0r0.us.auth0.com/.well-known/jwks.json",
  }),
  audience: "http://www.challenges-api.com",
  issuerBaseURL: "https://dev-hc8euv12o0fyh0r0.us.auth0.com/",
  algorithms: ["RS256"],
});

app.get("/", function (req, res) {
  res.json({
    health: "Service in Good Health",
    date: new Date(),
  });
});

app.use(jwtCheck);

app.get("/challenges", guard.check(["read:challenges"]), function (req, res) {
  console.log(`Actual api is being called for result ------- `);
  res.json({
    challenge1: "This is the first challenge",
    challenge2: "This is another challenge",
  });
});

app.listen(port);

console.log("Running on port ", port);
