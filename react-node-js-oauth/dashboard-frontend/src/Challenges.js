import React, { useState, useEffect } from "react";
import "./Challenges.css";

import queryString from "query-string";

const Challenges = ({ location }) => {
  const { code } = queryString.parse(location.search);
  const [challengesData, setChallengesData] = useState("none");

  useEffect( () =>  {
  //   console.log(` Got the Code -----------------`+ code);
   fetch(`http://localhost:3001/challenges?code=${code}` , {
      "mode" : 'no-cors'
    /*   method: 'GET',
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      }, */
    })
    .then(response => response.json())
    .catch(error => console.error('Error:', error));
  
fetch('https://dummyjson.com/products/1')
.then(res => res.json())
// .then(json => console.log(json))
.then(res => setChallengesData(JSON.stringify(res)))
  }, [code]);

  return (
    <div className="Challenges-body">
      <h3>Challenges</h3>
      <h5 className="Content">{challengesData}</h5>
    </div>
  );
};

export default Challenges;
