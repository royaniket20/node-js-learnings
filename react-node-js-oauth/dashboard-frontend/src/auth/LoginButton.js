import React from "react";
const LoginButton = () => {
  const login = async () => {
    const domain = "dev-hc8euv12o0fyh0r0.us.auth0.com";
    const audience = "http://www.challenges-api.com";
    const scope = "read:challenges";
    const clientId = "jM5XfnjqkQkiITXbOkduMqNq0ntMTtfW";
    const responseType = "code";
    const redirectUri = "http://localhost:3000/challenges";

    //  const response = await fetch(
    //     `https://${domain}/authorize?` +
    //      `audience=${audience}&` +
    //      `scope=${scope}&` +
    //     `response_type=${responseType}&` +
    //     `client_id=${clientId}&` +
    //     `redirect_uri=${redirectUri}`, {
    //       redirect: "manual"
    //     }
    //   );

    //window.location.replace(response.url);
    let url =
      `https://${domain}/authorize?` +
      `audience=${audience}&` +
      `scope=${scope}&` +
      `response_type=${responseType}&` +
      `client_id=${clientId}&` +
      `redirect_uri=${redirectUri}`;
    window.location.replace(url);
  };

  return (
    <button className="Login-button" onClick={() => login()}>
      Log In
    </button>
  );
};

export default LoginButton;
