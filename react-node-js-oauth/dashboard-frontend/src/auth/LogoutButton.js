import React from "react";

const LogoutButton = () => {
  const logout = async () => {
    const domain = "dev-hc8euv12o0fyh0r0.us.auth0.com";
    const clientId = "MoJ9Kym5zZX82XLjh7i7d9gjySZvpDVZ";
    const returnTo = "http://localhost:3000";

    const response = await fetch(
      `https://${domain}/logout?client_id=${clientId}&returnTo=${returnTo}`,
      { redirect: "manual" }
    );

    window.location.replace(response.url);
  };

  return (
    <button className="Login-button" onClick={() => logout()}>
      Log out
    </button>
  );
};

export default LogoutButton;
