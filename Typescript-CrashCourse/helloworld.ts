function sum(a: number, b: number) {
  return a + b;
}

sum(10, 20);

const sub = (a: number, b: number, c: number) => {
  return a - b;
};

sub(10, 20, 30);

sub(10, 20, "30"); // It will give tsc compilation error  - But still will create JS file 
