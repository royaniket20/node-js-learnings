// tsc watcher.ts --watch
//Basic JS trypes using typescript
console.log("Hello");
let b: boolean = false;
let age: Number = 56;
let fullname: string = "Aniket";
let fullnameMine: string = ` My Name is ${fullname}`;

let pets: Array<string> = ["a", "b"];
let ins: string[] = ["a", "b"];

let obj: object = {
  a: "name",
};

let meh: undefined = undefined;
let ahh: null = null;

//Sugar coated Typescript types

//Tuple
let basket: [string, number];
basket = ["Aniket", 23];
//Enum
enum Direction {
  Up = "UP",
  Down = "DOWN",
  Left = "LEFT",
  Right = "RIGHT",
}
let dir: Direction = Direction.Up;
console.log(dir);

// Use of Any - It can be of any type - No point of using it its neglect all ts benefit
//any is a middle ground during JS to TS migration
let whatever: any = "Aniket";
whatever = true;

//Void Type

let sing = (): void => {
  console.log("la la la ");
};
sing();
let dance = (a: number): string => {
  console.log("la la la ");
  return "la la la " + a;
};
console.log(dance(23));

//never type  - A function never return or have a  reachable endpoint

//This is correct usage
let error = (): never => {
  throw Error("I am error");
};
//Bad usage
// let  pong   = () : never =>{
//   console.log('la la la ');
// }

//Interface -----------
// interface Robot  {
//   count : number ;
//   type : string;
//   magic : boolean;
// }

type Robot = {
  count: number;
  type: string;
  magic: boolean;
};

let myRobo = function (robo: Robot) {
  console.log(`Robot Data - ${JSON.stringify(robo)}`);
};

myRobo({ count: 23, type: "Aniket", magic: true });
//we can use type insted of interface - But that do not create a structure name

/**
 * https://medium.com/@martin_hotell/interface-vs-type-alias-in-typescript-2-7-2a8f1777af4c
 * “One difference is, that interfaces create a new name that is used everywhere. Type aliases don’t create a new name — for instance, error messages won’t use the alias name.”
 * 2. “A second more important difference is that type aliases cannot be extended or implemented from”
 */

//Type assertion
//https://basarat.gitbook.io/typescript/type-system/type-assertion
interface Pets {
  count: number;
  type: string;
  magic: boolean;
  poochi?: string; //Optional Parameter
}
let dog = {} as Pets;
dog.count; // accsiable Now as it is inferred as Pets

//Function  - Creation and usage
let myRobo1 = function (robo: Pets) {
  console.log(`Pet  Data - ${JSON.stringify(robo)}`);
};
myRobo1({ count: 23, type: "Aniket", magic: true });

//Class
class Animal {
  private sing: string; //By Default member variable and functions are public
  constructor(sing: string) {
    this.sing = sing;
  }
  greet() {
    console.log("Hurrrrr - " + this.sing);
  }
}

let lion = new Animal("Bl bla ");
lion.greet();

//Union  - allowed any of the given types
let confused: string | number | boolean = 123;

//Typescript can do the type inference also 
let x = 4;
//x = 'Hello'; //Error - TS inferred it is a number variable 