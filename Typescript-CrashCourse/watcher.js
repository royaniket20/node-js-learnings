// tsc watcher.ts --watch
//Basic JS trypes using typescript
console.log("Hello");
var b = false;
var age = 56;
var fullname = "Aniket";
var fullnameMine = " My Name is ".concat(fullname);
var pets = ["a", "b"];
var ins = ["a", "b"];
var obj = {
    a: "name",
};
var meh = undefined;
var ahh = null;
//Sugar coated Typescript types
//Tuple
var basket;
basket = ["Aniket", 23];
//Enum
var Direction;
(function (Direction) {
    Direction["Up"] = "UP";
    Direction["Down"] = "DOWN";
    Direction["Left"] = "LEFT";
    Direction["Right"] = "RIGHT";
})(Direction || (Direction = {}));
var dir = Direction.Up;
console.log(dir);
// Use of Any - It can be of any type - No point of using it its neglect all ts benefit
//any is a middle ground during JS to TS migration
var whatever = "Aniket";
whatever = true;
//Void Type
var sing = function () {
    console.log("la la la ");
};
sing();
var dance = function (a) {
    console.log("la la la ");
    return "la la la " + a;
};
console.log(dance(23));
//never type  - A function never return or have a  reachable endpoint
//This is correct usage
var error = function () {
    throw Error("I am error");
};
var myRobo = function (robo) {
    console.log("Robot Data - ".concat(JSON.stringify(robo)));
};
myRobo({ count: 23, type: "Aniket", magic: true });
var dog = {};
dog.count; // accsiable Now as it is inferred as Pets
//Function  - Creation and usage
var myRobo1 = function (robo) {
    console.log("Pet  Data - ".concat(JSON.stringify(robo)));
};
myRobo1({ count: 23, type: "Aniket", magic: true });
//Class
var Animal = /** @class */ (function () {
    function Animal(sing) {
        this.sing = sing;
    }
    Animal.prototype.greet = function () {
        console.log("Hurrrrr - " + this.sing);
    };
    return Animal;
}());
var lion = new Animal("Bl bla ");
lion.greet();
//Union  - allowed any of the given types
var confused = 123;
//Typescript can do the type inference also 
var x = 4;
//x = 'Hello'; //Error - TS inferred it is a number variable 
