const https = require("https");
const fs = require("fs");
const path = require("path");
const express = require("express");
const helmet = require("helmet");
require("dotenv").config();
const passport = require("passport");
const cookieSession = require("cookie-session");
const { Strategy } = require("passport-google-oauth20");
const PORT = 3000;

const config = {
  CLIENT_ID: process.env.CLIENT_ID,
  CLIENT_SECRET: process.env.CLIENT_SECRET,
  COOKIE_KEY_1: process.env.COOKIE_KEY_1,
  COOKIE_KEY_2: process.env.COOKIE_KEY_2,
};

function verifyCallback(accessToken, refreshToken, profile, done) {
  //We can save the proifile Info in our App Db
  console.log(`Google Access Token : ${accessToken}`);
  console.log(`Google Refresh Token : ${refreshToken}`);
  console.log(`Google Profile Info - \n : ${JSON.stringify(profile)}`);
  done(null, profile); // We can sen Error as First param if Some validation failed
}
//preparing Passport Strategy
passport.use(
  new Strategy(
    {
      callbackURL: "/auth/google/callback",
      clientSecret: config.CLIENT_SECRET,
      clientID: config.CLIENT_ID,
    },
    verifyCallback
  )
);
//When save session to Cookie
passport.serializeUser((user, done) => {
  console.log(`User is being serilized `);
  done(null, user.id); //We can Just Keep what is needed not the whole package
});
//Getting the sesison from cookie
passport.deserializeUser((id, done) => {
  console.log(`User is being de-serilized `); //Only Id is there as we choose only Id
  done(null, id);
});

const app = express();
//Helmet for all Routes
app.use(helmet());
//Setup Cookie
app.use(
  cookieSession({
    name: "session",
    maxAge: 1000 * 60 * 60 * 24,
    keys: [config.COOKIE_KEY_1, config.COOKIE_KEY_2], //Two Keys for key rotation
  })
);
//auth middle wares
app.use(passport.initialize()); //Setup Passport Middleware | It setup Session also
app.use(passport.session()); //Validate the Existing Session
function isUserLoggedIn(req, resp, next) {
  //req.user have security context
  console.log(`Calling isUserLoggedIn.........`);
  const isLoggedIn = req.isAuthenticated() && req.user; // isAuthenticated is coming from passport
  if (!isLoggedIn) {
    return resp.status(401).json({ msg: "User is Not logged In" });
  }
  next();
}

function isUserAuthorized(req, resp, next) {
  console.log(`Calling isUserAuthorized.........`);
  const isAuthorized = true;
  if (!isAuthorized) {
    return resp.status(403).json({ msg: "User is Not Authorized" });
  }
  next();
}
app.get("/", (req, res) => {
  console.log(`Calling the Root Path .....`);
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

//Google Login
app.get(
  "/auth/google/login",
  passport.authenticate("google", {
    scope: ["email"],
  }),
  (req, res) => {
    console.log(`Calling the Google Login Url .....`);
  }
);

//Google Login success clabback
app.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    successRedirect: "/",
    faliureRedirect: "/faliure",
    session: true, //Now session will be saved
  }),
  (req, res) => {
    console.log(`Google Oauth Auth is successful .....`);
  }
);

//Common Logout
app.get("/auth/logout", (req, res) => {
  console.log(`Calling the Common Logout Url .....`);
  req.logOut(); // Remove req.user and clear logged in session
  //  res.send("User has been Logged Out!!");
  res.redirect("/");
});

//Common Login Faliure Endpoint
app.get("/failure", (req, res) => {
  console.log(`Calling the Common Logout Url .....`);
  res.send("User has been failed to Login ");
});
//Adding Middle ware in Sequence
app.get("/secret", isUserLoggedIn, isUserAuthorized, (req, res) => {
  console.log(`Calling the Root Path .....`);
  res.send("This is a secret value !!");
});

// app.listen(PORT, () => {
//   console.log(`Listening on port ${PORT}...`);
// });
https
  .createServer(
    {
      key: fs.readFileSync("./certs/rootCAKey.pem"),
      cert: fs.readFileSync("./certs/rootCACert.pem"),
    },
    app
  )
  .listen(PORT, () => {
    console.log(`Listening on port ${PORT}...`);
  });
