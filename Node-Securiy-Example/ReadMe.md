#Generate the private key of the root CA:
openssl genrsa -out rootCAKey.pem 2048
Generate the self-signed root CA certificate:
openssl req -x509 -sha256 -new -nodes -key rootCAKey.pem -days 3650 -out rootCACert.pem