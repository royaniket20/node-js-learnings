const mission = process.argv[2];

if (mission === "learn") {
  console.log(`Time to learn some Node Js program`);
} else {
  console.log(`Is there more interesting Mission : ${mission}`);
}
/**
 * The process object provides information about, and control over, the current Node.js process.
 *
 * The process.argv property returns an array containing the command-line arguments passed when the Node.js process was launched. The first element will be process.execPath. See process.argv0 if access to the original value of argv[0] is needed. The second element will be the path to the JavaScript file being executed. The remaining elements will be any additional command-line arguments.
 */

/**
 * >node process-example.js "Space-Walk"
Is there more interesting Mission : Space-Walk
 * 
 */
