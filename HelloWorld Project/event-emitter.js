const EventEmitter = require("events");
//Subject
const celeb = new EventEmitter();

//Callbacks
let winEventCallBack1 = () => {
  console.log(`Race Win Happened`);
};
let winEventCallBack2 = () => {
  console.log(`Prize Collection Happened`);
};

let winEventCallBack3 = (data) => {
  console.log(`Prize Collection Happened of amount - ${data}`);
};


//Subscriber  to Event

celeb.on("Race Win", winEventCallBack1);
celeb.on("Race Win", winEventCallBack2);
celeb.on("Race Win", winEventCallBack3);

//Event Emitting
celeb.emit("Race Win", 100);
celeb.emit("Race Lost"); //No Listner

//Event Process obj is based on EventEmitter
let printExitCallBack = (statusCode) => {
  console.log(`Program Exited : Code = ${statusCode}`);
};
process.on("exit", printExitCallBack);
