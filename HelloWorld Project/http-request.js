let https = require('https');



let processResult = function (callEventEmitter){

  callEventEmitter.on("data" , (chunkOfData) =>{
    console.log(`Fetched Data - ${chunkOfData}`);
  })

  callEventEmitter.on("end" , () =>{
    console.log(`Fetched Data ended !`);
  })
}
let req = https.request('https://jsonplaceholder.typicode.com/todos/1' , processResult)


//This will actually call the Request 
req.end();