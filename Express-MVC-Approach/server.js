const express = require("express"); //this is a Function Import
const path = require("path");
const messageController = require("./controller/messages.controller.js");
const friendsRouter = require("./routes/frineds.router.js");
const app = express(); //Calling the Function yo get the Event

app.set("view engine", "hbs");
app.set("views", path.join(__dirname, "public/templates"));
const PORT = 3000;

app.listen(PORT, () => {
  console.log(`Server started on ${PORT}`);
});

app.get("/", messageController.sendHello);

app.get("/json", messageController.sendMessage);

app.get("/photo", messageController.sendPhoto);

//Write our Own Middle Ware

app.use((req, resp, next) => {
  console.log(`I am inside  middleware 1`);
  const start = Date.now();
  next(); //Here Now this will go to actual endpoint for processing
  const end = Date.now();
  console.log(`${req.url} --> ${req.method} | Time taken - ${end - start} ms`);
  console.log(`I am coming out of  middleware 1`);
});

app.use((req, resp, next) => {
  console.log(`I am inside  middleware 2`);
  next(); //Here Now this will go to actual endpoint for processing
  console.log(`I am coming out of middleware 2`);
});

//Json parsing middleware - Built In
app.use(express.json());
//Mount the Router and Minimuze Path common portion
// app.use(friendsRouter)

app.use("/friends", friendsRouter);

//Adding Middle ware for service static site  - Here we are adding Middle ware for the specific path
app.use("/static", express.static("./public"));

 //So we know here all static Resources are starting with /static 

 /**
  * Because public/templates folder have a layout.hbs that will kick In first 
  * Then inside it when It find {{{body}}} tag --It will go to Index.hbs to render -
  * then It will return Back 
  */
 app.use("/templated/site" , (req , resp)=>{
  resp.render("index" , {
    title_value : 'Template',
    message_h3 : 'Hayout 1 Message',
    heading_value : 'Some Dynamic Heading'
  })
 });

 //Some layout Example 
