
const express = require("express"); 
//Use of Router 
const friendsRouter = express.Router();
const friendsController = require("../controller/friends.controller.js");
// friendsRouter.get("/friends", friendsController.getfriends);

// friendsRouter.get("/friends/:firendId", friendsController.getFirendById);

// friendsRouter.post("/friends", friendsController.addFriend);


//Adding custom Middle ware Only for this Route Group i.e Router 

friendsRouter.use((req, resp, next) => {
  console.log(`I am inside  middleware 3 via FiredRouter - ${req.ip}`);
  next(); //Here Now this will go to actual endpoint for processing
  console.log(`I am coming out of middleware 3 via FriendRouter`)});

friendsRouter.get("/", friendsController.getfriends);

friendsRouter.get("/:firendId", friendsController.getFirendById);

friendsRouter.post("/", friendsController.addFriend);


module.exports = friendsRouter;