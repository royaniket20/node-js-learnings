const path = require("path") //For File path handling for all our OS 

function sendHello(request, response) {
  console.log(`Callig - ${request.url}`);
  response.send("Hello !!!");
}

function sendMessage(request, response) {
  console.log(`Callig - ${request.url}`);
  response.send({ id: 100, name: "Aniket Roy" }); //Automatically add suitable  response content type
}

function sendPhoto(request, response) {
  console.log(`Callig - ${request.url}`);
  let pathData = path.join(__dirname ,"..", "publicPhotos" ,"snowMountain.jpg" );
  console.log(`Getting photo from Path - ${pathData} `);
  response.sendFile(pathData); //Automatically add suitable  response content type
}

module.exports = {
  sendHello,
  sendMessage,
  sendPhoto
};
