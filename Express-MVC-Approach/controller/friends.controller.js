const friendModel = require("../models/friends.models");

function getfriends(request, response) {
  console.log(`Callig - ${request.baseUrl}${request.url}`);
  response.send(friendModel); //Automatically add suitable  response content type
}
/*** req.baseUrl is used for giving you base Url 
req.url - give you current Url 
 **/
function getFirendById(request, response) {
  console.log(`Callig - ${request.baseUrl}${request.url}`);
  let firendId = Number(request.params.firendId);
  let friend = friendModel.find((item) => item.id === firendId);
  if (friend) {
    response.send(friend);
  } else {
    response
      .status(404)
      .json({ msg: "Friend Not Found with Id - " + request.params.firendId });
  }
  //Automatically add suitable  response content type
}

function addFriend(request, response) {
  console.log(`Callig - ${request.baseUrl}${request.url}`);
  const newFriend = {
    id: friendModel.length,
    name: request.body.name,
  };
  console.log(`New Friend ADDED  -  ${JSON.stringify(newFriend)}`);
  friendModel.push(newFriend);
  if (request.body.name) {
    response.send(newFriend);
  } else {
    response
      .status(400)
      .json({ msg: "Invalid Payload Sent- " + JSON.stringify(request.body) });
  }
  //Automatically add suitable  response content type
}

module.exports = {
  getfriends,
  getFirendById,
  addFriend,
};
