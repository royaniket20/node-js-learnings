const {
  getAllProducts,
  productsByPriceRange,
  productsById,
  addNewProduct,
  addNewProductReview,
} = require("./product.model");
const { PubSub, withFilter } = require("graphql-subscriptions");
const pubsub = new PubSub();
module.exports = {
  Subscription: {
    productUpdated: {
      subscribe: () =>
        pubsub.asyncIterator(["Product_Review_Updated", "Product_New_Added"]),
      resolve: (payload) => {
        console.log(payload + "********************");
        return JSON.parse(payload);
      },
    },
  },
  Mutation: {
    addNewProduct: (_, args, context, info) => {
      console.log(`Adding new Product`);
      let newProduct = addNewProduct(args.description, args.price);
      console.log(
        `Publish New  Product to pubsub : ${JSON.stringify(newProduct)}`
      );
      pubsub.publish("Product_New_Added", JSON.stringify(newProduct)); //Async in nature
      return newProduct;
    },
    addNewProductReview: (_, args, context, info) => {
      console.log(`Adding new Review to a Existing Product`);

      let reviewAdded = addNewProductReview(args.id, args.rating, args.comment);
      let updatedProduct = productsById(args.id);
      console.log(
        `Publish Updated Product to pubsub : ${JSON.stringify(updatedProduct)}`
      );
      pubsub.publish("Product_Review_Updated", JSON.stringify(updatedProduct)); //Async in nature
      return reviewAdded;
    },
  },
  Query: {
    //async can be used or we can return promise
    products: (parent, args, context, info) => {
      console.log(`Calling products resolver `);
      return Promise.resolve(getAllProducts());
    },
    //Rule is in resolver if you are not using that argument - mark it using _
    productsByPriceRange: (_, args, __) => {
      console.log(`Calling product filter by price range`);
      return Promise.resolve(
        productsByPriceRange(args.minPrice, args.maxPrice)
      );
    },
    product(_, args, __) {
      console.log(`Calling product filter by Id`);
      return productsById(args.id);
    },
  },
};
