     const {getAllOrders} = require("./order.model")
     module.exports = {
        Query: {
      orders: async (parent, args, context, info) => {
        console.log(`Calling orders resolver `);
        return await Promise.resolve(getAllOrders());
      }
    }
}