const mongoose = require("mongoose");

const DB_SERVER_URL =
"mongodb+srv://admin:admin@subscriptioncluster.bmcgw0s.mongodb.net/?retryWrites=true&w=majority";
mongoose.connection.once("open", () => {
  console.log(`Mongo Server is Connected !!!`);
});
mongoose.connection.on("error", () => {
  console.error(`Something bad happend on Mongo Connection `);
});

async function connectToMongo() {
  console.log(`Connect the Mongo Database`);
  await mongoose.connect(DB_SERVER_URL);
}

async function disconnectFromMongo() {
  console.log(`connection closed !`);
  await mongoose.disconnect();
  await mongoose.connection.close();
}

module.exports = {
  connectToMongo,
  disconnectFromMongo,
};
