const express = require("express");
const path = require("path");
const { createServer } = require("http"); //We will use this so that we can handle both express app http  and websocket both
const { ApolloServer } = require("apollo-server-express");
const { makeExecutableSchema } = require("@graphql-tools/schema");
const { loadFilesSync } = require("@graphql-tools/load-files");
const { connectToMongo, disconnectFromMongo } = require("./mongoConfig");
const { SubscriptionServer } = require("subscriptions-transport-ws");
const { subscribe, execute } = require("graphql");

async function startServer() {
  const app = express();
  const server = createServer(app);
  const PORT = 3000;
  console.log(`Application will be Running on Server PORT - ${PORT}`);

  console.log(`Starting the Node Server`);
  await connectToMongo();
  console.log(`making preparation for graphql Schema `);
  const typeArr = loadFilesSync(path.join(__dirname, "**/*.graphql"));
  const resolverArr = loadFilesSync(path.join(__dirname, "**/*.resolver.js"));
  const schemaDefination = makeExecutableSchema({
    typeDefs: typeArr,
    resolvers: resolverArr,
  });
  console.log(`preparing subscription server ...`);
  const subsserver = SubscriptionServer.create(
    {
      schema: schemaDefination,
      execute: execute,
      subscribe: subscribe,
    },
    {
      server: server,
      path: "/graphql",
    }
  );
  console.log(`create apollo server using schmea Def`);
  const apolloServer = new ApolloServer({
    schema: schemaDefination,
    plugins: [
      {
        async serverWillStart() {
          return {
            async drainServer() {
              subsserver.close();
              disconnectFromMongo();
            },
          };
        },
      },
    ],
  });
  console.log(`Awating apollo server instance to start`);
  await apolloServer.start();
  console.log(`Apollo Server started successfully`);
  console.log(`Apply Middleware to the server for a specific path`);
    apolloServer.applyMiddleware({
        app,
        path: "/graphql",
      });
  server.listen(PORT, () => {
    console.log(`Server started at Port : ${PORT}`);
  });
  console.log(`Server Now Started !!! `);
}

startServer();
