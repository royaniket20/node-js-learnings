function decrypt(data) {
  return `Decrypted Data - ${data}`;
}

function read() {
  return decrypt("Data");
}

//module keyword can be used to export
module.exports = {
  read,
};

console.log(`Initalizing the Response Module`);