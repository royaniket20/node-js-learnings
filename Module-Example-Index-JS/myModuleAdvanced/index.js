console.log(`Index js file called as Folder is imported as Module `);
const request = require("./request.js");
const response = require("./response.js");

module.exports = {
  send: request.send,
  read: response.read,
  REQUEST_TIMEOUT: request.REQUEST_TIMEOUT,
  log: require("./singleElementExport.js"),
};
