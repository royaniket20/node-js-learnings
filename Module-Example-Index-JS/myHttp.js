// const { send, REQUEST_TIMEOUT } = require("./myModule/request.js");
// const resp = require("./myModule/response");
// const log = require("./myModule/singleElementExport.js");
// const { READ_TIMEOUT } = require("./myModule/request.js");

const { request , response , log } = require("./myModule"); //Now this need index.js in thois Folder
//Here Log is imported as Function Directly 
function myRequest(data, url) {
  request.send(url, data);
  return response.read();
}
log(`TimeOut - ${request.REQUEST_TIMEOUT}`);
log(myRequest("Url", "Some Data"));
