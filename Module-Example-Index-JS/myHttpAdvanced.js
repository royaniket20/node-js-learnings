// const { send, REQUEST_TIMEOUT } = require("./myModule/request.js");
// const resp = require("./myModule/response");
// const log = require("./myModule/singleElementExport.js");
// const { READ_TIMEOUT } = require("./myModule/request.js");

const { send , read ,REQUEST_TIMEOUT, log } = require("./myModuleAdvanced"); //Now this need index.js in thois Folder
//Here Log is imported as Function Directly 
function myRequest(data, url) {
  send(url, data);
  return read();
}
log(`TimeOut - ${REQUEST_TIMEOUT}`);
log(myRequest("Url", "Some Data"));
