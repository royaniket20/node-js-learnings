//An Detailed Explanation How promise works

const express = require("express");
const app = express();

app.get("/", (req, resp) => {
  resp.send({ msg: "Performance Example" });
});

function delay(duration) {
  const startTime = Date.now();
  while (Date.now() - startTime < duration);
}

app.get("/timer", (req, resp) => {
  delay(5000); //This will Block the JS Main thread
  resp.send({ msg: "I have waked Up " });
});

app.get("/timerImproved", (req, resp) => {
  // Executor function passed to the
  // Promise constructor as an argument
  let executorFunction = function (resolve, reject) {
    // Your logic goes here...
    // Make an asynchronous call and either resolve or reject
    console.log(`I am executing something in Promise`);
    delay(2000); //No way you can Improve this - The moment it will load in call stack It will block It
    console.log(`Now I have finsihed and goig to call resolve or reject`);
    throw new Error("Something Bad happened!!"); //If this get executed - error resolver or catch handler will handle it
    resolve("I am Done");
    reject("Something Bad happend!"); //This will never get called Only one call Possible
    delay(1000); //No way you can Improve this - The moment it will load in call stack It will block It
    console.log(`I have called Reject or Resolve `); //Until this Line is executed None of the Reject/Resolve method will be called
  };
  //executor function that runs automatically when a new Promise is created.
  let promise = new Promise(executorFunction); //The new Promise() constructor returns a promise object.
  // the returned promise object should be capable of informing when the execution has been started, completed (resolved) or retuned with error (rejected).
  /**
 * 
 * A function which return Promise need not to be Async mentioned 
 * However if a Function is mentioned as async - It will always return a Promise 
 * https://www.freecodecamp.org/news/javascript-promise-tutorial-how-to-resolve-or-reject-promises-in-js/
A promise object has the following internal properties:

1.state – This property can have the following values:
  pending: Initially when the executor function starts the execution.
  fulfilled: When the promise is resolved.
  rejected: When the promise is rejected.
2.result – This property can have the following values:
  undefined: Initially when the state value is pending.
  value: When resolve(value) is called.
  error: When reject(error) is called.

  These internal properties are code-inaccessible but they are inspectable.
  A promise's state can be pending, fulfilled or rejected. A promise that is either resolved or rejected is called settled.

  A Promise executor should call only one resolve or one reject. Once one state is changed (pending => fulfilled or pending => rejected), that's all.
   Any further calls to resolve or reject will be ignored.

   The handler methods, .then(), .catch() and .finally(), help to create the link between the executor and the consumer functions so that they can be in sync when a promise resolves or rejects.

//THEN Metod 

The .then() method  accepts two functions as parameters
If you are interested only in successful outcomes, you can just pass one argument
If you are interested only in the error outcome, you can pass null for the first argument
However, you can handle errors in a better way using the .catch() method

If we throw an Error like new Error("Something wrong!")  instead of calling the reject from the promise executor , it will still be treated as a rejection

The .finally() handler performs cleanups like stopping a loader, closing a live connection, and so on. The finally() method will be called irrespective of whether a promise resolves or rejects. 
It passes through the result or error to the next handler which can call a .then() or .catch() again.


   The  promise.then() call always returns a promise. This promise will have the state as pending and result as undefined. It allows us to call the next .then method on the new promise.
 */
  function handleResolve(result) {
    console.log(`I am resolved - ${result}`);
  }

  function handleReject(reject) {
    console.log(`I am rejected - ${reject}`);
  }

  function handleCatch(reject) {
    console.log(`I am rejected handled via catch - ${reject}`);
  }

  function finallyHandler() {
    console.log(`I am Finally settiled `);
    resp.send({ msg: "I have waked Up as soon possible" });
  }
  //Here  both catch handler and reject resolver will fire if error happen
  // promise.then(handleResolve, handleReject);
  // promise.catch(handleCatch);
  // promise.finally(finallyHandler);

  //Here  Only  catch handler will fire but  reject resolver will not fire if error happen
  promise
    .then(handleResolve, handleReject)
    .catch(handleCatch)
    .finally(finallyHandler);
  //Here as errorresolver is ommited - catch block will be used
  // promise.then(handleResolve, null).catch(handleCatch).finally(finallyHandler);
});

app.listen(3000, () => {
  console.log(`Performance Server started at 3000 !`);
});
