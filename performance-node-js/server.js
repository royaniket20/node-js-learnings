const express = require("express");
const cluster = require("cluster");
const os = require("os");
const app = express();

app.get("/", (req, resp) => {
  resp.send({ msg: `Performance Example ${process.pid}  From Worker Node` });
});

function delay(duration) {
  const startTime = Date.now();
  while (Date.now() - startTime < duration);
}

app.get("/timer", (req, resp) => {
  delay(1000); //This will Block the JS Main thread
  resp.send({ msg: `I have waked Up  ${process.pid}` });
});
//If using pm2 - No need to write clustering Logic
/* if (cluster.isMaster) {
  //Will get executed when First time server.js get executed on app startup
  console.log(`Master node started - Now Forking Cluster Nodes`);
  let cores = os.cpus().length //Nuber of Logical Cores in your serever 
  for (let index = 0; index <cores; index++) {
    cluster.fork();
  }
} else { */
console.log(`Start Listening to traffic from Forked Node`);
app.listen(3000, () => {
  console.log(`Performance Server started at 3000 !`);
});
/* } */
